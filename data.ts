import { Entity, GameSleeper, Rectangle, Roshan, RoshanSpawner } from "wrapper/Imports"
import { PathX } from "X-Core/Imports"

export class ROSHAN_DATA  {

	public static KILL_TIME = 0
	public static ROSHAN: Nullable<Roshan>
	public static COUNT_DEATGS: number = 1
	public static COUNT_ATTACKS: number = 14
	public static SLEEPER = new GameSleeper()
	public static SPAWNER: Nullable<RoshanSpawner>
	public static ICON_POSITION = new Rectangle()

	public static PICK_UP_TIME_EGIDA: number = 0
	public static DROPS: string[] = ["item_aegis"]
	public static ATTACKER: [Entity, number][] = []

	public static SoundPath: string[] = [
		"sounds/weapons/creep/roshan/dt/scream.vsnd_c",
		"sounds/weapons/creep/roshan/bash.vsnd_c",
		"sounds/weapons/creep/roshan/dt/candy_or_else.vsnd_c",
		"sounds/weapons/creep/roshan/dt/death_seq_2.vsnd_c",
		"sounds/weapons/creep/roshan/dt/toss.vsnd_c",
	]

	public static Images: string[] = [
		PathX.Images.roshan_halloween_angry,
		PathX.Images.roshan_halloween_levels,
	]

	public static FullDispose() {
		this.ATTACKER = []
		this.KILL_TIME = 0
		this.COUNT_DEATGS = 1
		this.ROSHAN = undefined
		this.SPAWNER = undefined
		this.PICK_UP_TIME_EGIDA = 0
		this.DROPS = ["item_aegis"]
		this.SLEEPER.FullReset()
	}
}
