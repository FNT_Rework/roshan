import { GameRules, GameState, LocalPlayer, Team } from "wrapper/Imports"
import { State } from "../menu"

export class ROSHAN_VALIDATE {
	public static get IsInGame() {
		return State.value
			&& LocalPlayer !== undefined
			&& GameState.LocalTeam !== Team.Observer
			&& GameRules !== undefined
			&& GameRules.IsInGame
	}
}
