import { ArrayExtensions, Color, GameRules, GameState, GUIInfo, LocalPlayer, Menu, MinimapSDK, Rectangle, RendererSDK, Roshan, SoundSDK, Team, Vector2 } from "wrapper/Imports"
import { EntityX, GameX, PathX } from "X-Core/Imports"
import { ROSHAN_DATA } from "../data"
import { RoshanaltKey, RoshanPing, RoshanPingInteravl, RoshanPingSoundVolume, RoshanRemaining, RoshanshowMinTime, RoshanUseScan } from "../menu"
import { ROSHAN_UTIL } from "../Service/Util"

export class ROSHAN_PANEL {

	public static DrawItems(state: boolean = true, position: Rectangle, size: Vector2, screen: Vector2, gapScale: number) {
		if (!state)
			return position
		const items = ROSHAN_DATA.DROPS
		const roshan = ROSHAN_DATA.ROSHAN
		const itemsRec = position.Add(new Vector2(0, size.y - (gapScale * 2))).Clone()
		const center = itemsRec.Center
		if (roshan === undefined || roshan.Items.length <= 0) {
			// TODO
			if (ROSHAN_DATA.COUNT_DEATGS === 2 && !ROSHAN_DATA.DROPS.includes("item_cheese")) {
				if (!ROSHAN_DATA.DROPS.includes("item_aghanims_shard"))
					ROSHAN_DATA.DROPS.push("item_aghanims_shard")
			}

			if (ROSHAN_DATA.COUNT_DEATGS > 2) {
				if (!ROSHAN_DATA.DROPS.includes("item_cheese"))
					ROSHAN_DATA.DROPS.push("item_cheese")
				if (ROSHAN_DATA.DROPS.includes("item_aghanims_shard"))
					ArrayExtensions.arrayRemove(ROSHAN_DATA.DROPS, "item_aghanims_shard")
			}

			if (ROSHAN_DATA.COUNT_DEATGS === 3 && !ROSHAN_DATA.DROPS.includes("item_ultimate_scepter"))
				ROSHAN_DATA.DROPS.push("item_ultimate_scepter")
			if (ROSHAN_DATA.COUNT_DEATGS > 3 && !ROSHAN_DATA.DROPS.includes("item_refresher_shard"))
				ROSHAN_DATA.DROPS.push("item_refresher_shard")
		}

		const itemSizeScale = GUIInfo.ScaleHeight(20, screen)
		const leftPosition = new Vector2(center.x - ((itemSizeScale / 2) * items.length) - ((gapScale / 2) * (items.length - 1)), center.y)
		for (var i = 0; i < items.length; i++) {
			const pos = leftPosition.Add(new Vector2((i * itemSizeScale) + (i * gapScale), 0))
			RendererSDK.Image(PathX.DOTAItems(items[i]), pos, 0, new Vector2(itemSizeScale, itemSizeScale), Color.White)
		}
		return itemsRec
	}

	public static Respawn(rec: Rectangle, size: Vector2) {
		if (GameRules === undefined || ROSHAN_DATA.KILL_TIME === 0)
			return

		const gameTime = GameRules.GameTime
		const rawGameTime = GameState.RawGameTime
		const killTime = rawGameTime - ROSHAN_DATA.KILL_TIME
		const timeToEndPickUp = rawGameTime - ROSHAN_DATA.PICK_UP_TIME_EGIDA
		const position = rec.Add(new Vector2(0, rec.Height + (size.y / 2))).Clone()

		if (killTime <= 0)
			return

		let flag = RoshanRemaining.value
		if (RoshanaltKey.is_pressed)
			flag = !flag

		let Text = ""
		if (killTime > 480) {

			// prediction time
			let killTimePredict = 660 - killTime
			if (!flag)
				killTimePredict += gameTime

			Text = `${ROSHAN_UTIL.sToMin(killTimePredict)}*`

		} else if (ROSHAN_DATA.PICK_UP_TIME_EGIDA !== 0 && timeToEndPickUp <= 300) {
			let num4 = 300 - timeToEndPickUp
			if (!flag)
				num4 += gameTime
			Text = `${Menu.Localization.Localize("Aegis")}: ${ROSHAN_UTIL.sToMin(num4)}`
		} else {

			let num6 = 660 - killTime
			if (!flag)
				num6 += gameTime

			Text = ROSHAN_UTIL.sToMin(num6)

			if (RoshanshowMinTime.value)
				Text = `${ROSHAN_UTIL.sToMin((num6 - 180))} - ${Text}`
		}

		this.CenterTextRender(Text, position)
	}

	public static RoshanAttack(rec: Rectangle, size: Vector2) {

		const roshanPos = ROSHAN_DATA.SPAWNER
		const rawGameTime = GameState.RawGameTime
		const position = rec.Add(new Vector2(0, rec.Height + (size.y / 2))).Clone()

		ROSHAN_DATA.ATTACKER = ROSHAN_DATA.ATTACKER.filter(ar => rawGameTime - ar[1] < 2)

		if (ROSHAN_DATA.ATTACKER.length === 0)
			return

		const roshanHealth = Roshan.HP !== 0 ? Menu.Localization.Localize("HP: ")
			+ `${Math.floor(Roshan.HP)}/${Math.floor(Roshan.MaxHP)} | ${Math.floor(Math.floor(Roshan.HP) / Math.floor(Roshan.MaxHP) * 100)}%` : ""

		const RoshText = Menu.Localization.Localize("Roshan is under attack ") + roshanHealth

		this.CenterTextRender(RoshText, position, Color.White, 600)

		if (roshanPos === undefined || ROSHAN_DATA.SLEEPER.Sleeping("ROSHAN_ATTACK"))
			return

		if (RoshanUseScan.value && ROSHAN_DATA.COUNT_ATTACKS <= 0) {

			const alliesCountAlive = EntityX.AllyHeroes.filter(x => x.IsAlive)

			const scanCooldown = LocalPlayer!.Team === Team.Radiant
				? GameRules!.ScanCooldownRadiant
				: GameRules!.ScanCooldownDire

			if (alliesCountAlive.length >= 3
				&& scanCooldown === 0
				&& LocalPlayer !== undefined
				&& LocalPlayer.Hero !== undefined
				&& !ROSHAN_DATA.SLEEPER.Sleeping("ROSHAN_SCAN")
				&& !ROSHAN_DATA.SLEEPER.Sleeping("ROSHAN_ATTACK")
			) {
				LocalPlayer.Scan(ROSHAN_UTIL.RandomVector3(roshanPos.Position.Clone(), 50, 200))
				ROSHAN_DATA.SLEEPER.Sleep((GameX.Ping / 1000) + 500, "ROSHAN_SCAN")
			}
		}

		if (RoshanPingSoundVolume.value !== 0 && !ROSHAN_DATA.SLEEPER.Sleeping("ROSHAN_ATTACK")) {
			SoundSDK.PlaySound("sounds/ui/ping_attack.vsnd_c", (RoshanPingSoundVolume.value / 100))
			ROSHAN_DATA.SLEEPER.Sleep((RoshanPingInteravl.value * 1000), "ROSHAN_ATTACK")
		}

		if (!RoshanPing.value)
			return

		MinimapSDK.DrawPing(ROSHAN_UTIL.RandomVector3(roshanPos.Position.Clone(), 50, 100), Color.White, (rawGameTime + 10), roshanPos.Position.Length)
	}

	private static CenterTextRender(text: string, position: Rectangle, color: Color = Color.White, weight: number = 400) {
		const textSize = position.Height / 1.1
		const pos = Vector2.FromVector3(RendererSDK.GetTextSize(text, RendererSDK.DefaultFontName, textSize, weight))
			.MultiplyScalarForThis(-1)
			.AddScalarX(position.Width)
			.AddScalarY(position.Height)
			.DivideScalarForThis(2)
			.AddScalarX(position.x)
			.AddScalarY(position.y)
			.RoundForThis()
		RendererSDK.Text(text, pos, color, RendererSDK.DefaultFontName, textSize, weight)
	}
}
