import { Menu as MenuSDK, SoundSDK } from "wrapper/Imports"
import { ROSHAN_DATA } from "./data"

const Menu = MenuSDK.AddEntryDeep(["Visual", "Roshan"])
export const State = Menu.AddToggle("State", true)
const NotificatonTree = Menu.AddNode("Respawn")
export const StateChatSend = NotificatonTree.AddToggle("Chat", true, "send a chat to a message when a notification is clicked")
export const RSVolume = NotificatonTree.AddSlider("Volume", 2, 0, 100, 0, "sound volume as percents")
export const RSSoundType = NotificatonTree.AddDropdown("Sound type", [
	"Type #1",
	"Type #2",
	"Type #3",
	"Type #4",
	"Type #5",
])
NotificatonTree.AddButton("Play Sound").OnValue(() =>
	SoundSDK.PlaySound(ROSHAN_DATA.SoundPath[RSSoundType.selected_id], (RSVolume.value / 100)))

export const RoshanRemaining = Menu.AddToggle("Show remaining time", true, "Display remaining time first")
export const RoshanUseChat = Menu.AddToggle("Use chat", true, "When clicking on the roshan icon")
export const RoshanUseTranslateChat = Menu.AddDropdown("Language", ["Engilsh", "Russian"], 1, "Use chat language")

export const RoshanaltKey = Menu.AddKeybind("Bind", "Alt", "Switch remaining time to respawn")
export const RoshanshowMinTime = Menu.AddToggle("Min respawn time", true, "Display min time respawn roshan")
export const RoshanshowNextItems = Menu.AddToggle("Show items", true, "Show next drop items in roshan")

const RoshanMenuPing = Menu.AddNode("Settings ping")
export const RoshanUseScan = RoshanMenuPing.AddToggle("Use scan", false, "Used scan in roshan position for notify allies")
export const RoshanPingSoundVolume = RoshanMenuPing.AddSlider("Volume %", 1, 0, 100, 0, "Ping volume in percent (if 0 = disable)")
export const RoshanPingInteravl = RoshanMenuPing.AddSlider("Ping interval", 4, 4, 15, 0, "Intervals ping in roshan position")
export const RoshanPing = RoshanMenuPing.AddToggle("Ping in roshan", true, "Visual only for your")
