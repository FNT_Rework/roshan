import { Color, GameState, GUIInfo, Input, Rectangle, RendererSDK, Roshan, Vector2, VKeys } from "wrapper/Imports"
import { PathX } from "X-Core/Imports"
import { ROSHAN_DATA } from "../data"
import { RoshanshowNextItems } from "../menu"
import { ROSHAN_PANEL } from "../Service/Panel"

export const ROSHAN_PANEL_DRAW = () => {

	const screen = RendererSDK.WindowSize
	const borderColor = new Color(0, 224, 7)
	const roshanColor = new Color(219, 219, 219)

	const basePositon = GUIInfo.TopBar.TimeOfDayTimeUntil.Clone()
	const scaleW3 = GUIInfo.ScaleHeight(3, screen)
	const gapScale = GUIInfo.ScaleHeight(4, screen)
	const scaleW25 = GUIInfo.ScaleWidth(25, screen)
	const scaleW32 = GUIInfo.ScaleWidth(32, screen)
	const scaleH32 = GUIInfo.ScaleHeight(32, screen)

	const AliveOrIsPressedAlt = !Input.IsKeyDown(VKeys.MENU) && ROSHAN_DATA.KILL_TIME === 0
		&& !ROSHAN_DATA.SLEEPER.Sleeping("ROSHAN")

	if (Input.IsKeyDown(VKeys.MENU))
		basePositon.Add(new Vector2(0, scaleW25))

	if (AliveOrIsPressedAlt)
		return

	const dynamicPosition = basePositon.Clone()
	const size = new Vector2(scaleH32, scaleW32)
	const position = dynamicPosition.Center.SubtractScalarForThis(size.x / 2)

	ROSHAN_DATA.ICON_POSITION = new Rectangle(position, size)

	if (ROSHAN_DATA.SLEEPER.Sleeping("ROSHAN_ATTACK"))
		roshanColor.SetColor(200, 0, 0)

	if (ROSHAN_DATA.SLEEPER.Sleeping("ROSHAN_ATTACK")
		&& Input.CursorOnScreen.IsUnderRectangle(position.x, position.y, size.x, size.y))
		roshanColor.SetColor(255, 0, 0)

	if (!ROSHAN_DATA.SLEEPER.Sleeping("ROSHAN_ATTACK")
		&& Input.CursorOnScreen.IsUnderRectangle(position.x, position.y, size.x, size.y))
		roshanColor.SetColor(255, 255, 255)

	if (ROSHAN_DATA.KILL_TIME === 0) {
		RendererSDK.Arc(-90, 100, position, size, false, scaleW3, Color.Black)
		RendererSDK.Arc(-90, Math.floor(Roshan.HP / Roshan.MaxHP * 100), position, size, false, scaleW3, borderColor)
		RendererSDK.Image(PathX.Images.icon_roshan, position, -1, size, roshanColor)
	} else {
		const killTime = GameState.RawGameTime - ROSHAN_DATA.KILL_TIME // TODO
		RendererSDK.Arc(-90, 100, position, size, false, scaleW3, Color.Black)
		RendererSDK.Arc(-90, 100 * (killTime / 1000), position, size, false, scaleW3, (killTime > 480) ? Color.Yellow : Color.Red)
		RendererSDK.Image(PathX.Images.icon_roshan, position, -1, size, roshanColor)
	}

	const afterRec = ROSHAN_PANEL.DrawItems(RoshanshowNextItems.value, dynamicPosition, size, screen, gapScale)
	ROSHAN_PANEL.Respawn(afterRec, size)
	ROSHAN_PANEL.RoshanAttack(afterRec, size)
}
