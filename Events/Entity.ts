import { EventsSDK, Roshan, RoshanSpawner } from "wrapper/Imports"
import { ROSHAN_DATA } from "../data"

EventsSDK.on("EntityCreated", ent => {
	if (ent instanceof RoshanSpawner)
		ROSHAN_DATA.SPAWNER = ent

	if (ent instanceof Roshan)
		ROSHAN_DATA.ROSHAN = ent
})

EventsSDK.on("EntityDestroyed", ent => {
	if (ent instanceof RoshanSpawner)
		ROSHAN_DATA.SPAWNER = undefined

	if (ent instanceof Roshan)
		ROSHAN_DATA.ROSHAN = undefined
})
