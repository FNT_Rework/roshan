import { DOTA_GameState, EventsSDK, GameRules, NotificationsSDK } from "wrapper/Imports"
import { ROSHAN_DATA } from "../data"
import { RSSoundType, RSVolume, State, StateChatSend } from "../menu"
import { RoshanNotificaton } from "../Service/Notification"
import { ROSHAN_UTIL } from "../Service/Util"

EventsSDK.on("ParticleCreated", par => {
	if (GameRules?.GameState !== DOTA_GameState.DOTA_GAMERULES_STATE_GAME_IN_PROGRESS)
		return

	if (par.Path === "particles/neutral_fx/roshan_spawn.vpcf") {
		if (State.value)
			NotificationsSDK.Push(new RoshanNotificaton(StateChatSend.value,
				ROSHAN_DATA.SoundPath[RSSoundType.selected_id], (RSVolume.value / 100),
				ROSHAN_DATA.Images[Math.round(ROSHAN_UTIL.MtRand(0, ROSHAN_DATA.Images.length - 1))],
			))
		ROSHAN_DATA.KILL_TIME = 0
		ROSHAN_DATA.PICK_UP_TIME_EGIDA = 0
	}

	if (par.Path === "particles/items_fx/aegis_timer.vpcf" || par.Path === "particles/items_fx/aegis_respawn_timer.vpcf")
		ROSHAN_DATA.PICK_UP_TIME_EGIDA = 0
})
