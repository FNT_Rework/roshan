import { GameRules, GameState, Input, InputEventSDK, VMouseKeys } from "wrapper/Imports"
import { ROSHAN_DATA } from "../data"
import { RoshanaltKey, RoshanRemaining, RoshanUseChat, RoshanUseTranslateChat } from "../menu"
import { ROSHAN_UTIL } from "../Service/Util"
import { ROSHAN_VALIDATE } from "../Service/Validate"

InputEventSDK.on("MouseKeyDown", key => {
	if (!ROSHAN_VALIDATE.IsInGame || GameRules === undefined || !RoshanUseChat.value || key !== VMouseKeys.MK_LBUTTON)
		return true

	const rec = ROSHAN_DATA.ICON_POSITION
	const cursorPosition = Input.CursorOnScreen
	const cursorInRosh = cursorPosition.IsUnderRectangle(rec.x, rec.y, rec.pos2.x, rec.pos2.y)

	const gameTime = GameRules.GameTime
	const killTime = GameState.RawGameTime - ROSHAN_DATA.KILL_TIME
	const timeToEndPickUp = GameState.RawGameTime - ROSHAN_DATA.PICK_UP_TIME_EGIDA

	if (killTime <= 0)
		return

	let flag = RoshanRemaining.value
	if (RoshanaltKey.is_pressed)
		flag = !flag

	let TextChat = ""
	if (killTime > 480) {
		let killTimePredict = 660 - killTime
		killTimePredict += gameTime
		TextChat = `${ROSHAN_UTIL.sToMin(killTimePredict, true)}*`

	} else if (ROSHAN_DATA.PICK_UP_TIME_EGIDA !== 0 && timeToEndPickUp <= 300) {
		let num4 = 300 - timeToEndPickUp
		num4 += gameTime

		const lanuage = RoshanUseTranslateChat.selected_id === 0
			? "aegis"
			: "аега"

		TextChat = `${lanuage} ${ROSHAN_UTIL.sToMin(num4, true)}`

	} else {
		let num6 = 660 - killTime
		num6 += gameTime
		const lanuage = RoshanUseTranslateChat.selected_id === 0
			? "rs"
			: "рс"
		TextChat = `${lanuage} ${ROSHAN_UTIL.sToMin((num6 - 180), true)} ${ROSHAN_UTIL.sToMin(num6, true)}`
	}

	if (!cursorInRosh)
		return true

	if (ROSHAN_DATA.SLEEPER.Sleeping("ROSHAN_ATTACK"))
		GameState.ExecuteCommand(`chatwheel_say 53`)

	if (ROSHAN_DATA.KILL_TIME === 0)
		return true

	GameState.ExecuteCommand(`say_team ${TextChat}`)
	return false
})
