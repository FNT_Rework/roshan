import { DOTAGameUIState_t, EventsSDK, GameState } from "wrapper/Imports"
import { ROSHAN_PANEL_DRAW } from "../Module/Panel"
import { ROSHAN_VALIDATE } from "../Service/Validate"

EventsSDK.on("Draw", () => {
	if (!ROSHAN_VALIDATE.IsInGame || GameState.UIState !== DOTAGameUIState_t.DOTA_GAME_UI_DOTA_INGAME)
		return
	ROSHAN_PANEL_DRAW()
})
