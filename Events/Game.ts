import { Entity, EntityManager, EventsSDK, GameState, Hero, Roshan } from "wrapper/Imports"
import { ROSHAN_DATA } from "../data"
import { RoshanPingInteravl, State } from "../menu"

EventsSDK.on("GameEvent", (name, obj) => {
	if (!State.value)
		return
	if (name !== "entity_hurt" && name !== "entity_killed")
		return
	const ent1: Entity | number = EntityManager.EntityByIndex(obj.entindex_killed) || obj.entindex_killed,
		ent2: Entity | number = EntityManager.EntityByIndex(obj.entindex_attacker) || obj.entindex_attacker
	if (ent1 === undefined || ent2 === undefined
		|| (!(ent1 instanceof Hero && ent1.IsValid && !ent1.IsVisible) && !(ent2 instanceof Hero && ent2.IsValid && !ent2.IsVisible))
		|| (!(ent1 instanceof Roshan || ent1 === Roshan.Instance?.Index) && !(ent2 instanceof Roshan || ent2 === Roshan.Instance?.Index)))
		return
	const hero = ent1 instanceof Hero ? ent1 : ent2
	const ar = ROSHAN_DATA.ATTACKER.find(ar_ => ar_[0] === hero)
	if (ar === undefined) {
		ROSHAN_DATA.ATTACKER.push([ent1 instanceof Hero ? ent1 : ent2 as Entity, GameState.RawGameTime])
		ROSHAN_DATA.SLEEPER.Sleep((RoshanPingInteravl.value * 1000), "ROSHAN")
	} else {
		ar[1] = GameState.RawGameTime
		ROSHAN_DATA.SLEEPER.Sleep((RoshanPingInteravl.value * 1000), "ROSHAN")
		ROSHAN_DATA.COUNT_ATTACKS -= 1
	}
})
