import { DOTA_CHAT_MESSAGE, EventsSDK, GameState } from "wrapper/Imports"
import { ROSHAN_DATA } from "../data"

EventsSDK.on("ChatEvent", (type: DOTA_CHAT_MESSAGE) => {
	if (type !== DOTA_CHAT_MESSAGE.CHAT_MESSAGE_ROSHAN_KILL && type !== DOTA_CHAT_MESSAGE.CHAT_MESSAGE_AEGIS)
		return

	ROSHAN_DATA.SLEEPER.ResetKey("ROSHAN")
	ROSHAN_DATA.SLEEPER.ResetKey("ROSHAN_ATTACK")

	if (type === DOTA_CHAT_MESSAGE.CHAT_MESSAGE_ROSHAN_KILL) {
		ROSHAN_DATA.KILL_TIME = GameState.RawGameTime
		ROSHAN_DATA.ROSHAN = undefined
		ROSHAN_DATA.COUNT_ATTACKS = 7
		ROSHAN_DATA.COUNT_DEATGS += 1
		return
	}

	if (type !== DOTA_CHAT_MESSAGE.CHAT_MESSAGE_AEGIS)
		return

	ROSHAN_DATA.PICK_UP_TIME_EGIDA = GameState.RawGameTime
})
